﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(() => MainAsync(args)).Wait();
        }

        static async Task MainAsync(string[] args)
        {
            Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;

            City MyCity = new City(CityLength, CityWidth);
            Car car = MyCity.AddCarToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
            Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));

            Console.WriteLine($"Passenger initial position: X -  {passenger.GetCurrentXPos()} Y - {passenger.GetCurrentYPos()}");
            Console.WriteLine($"Passenger destination: X -  {passenger.DestinationPos.X} Y - {passenger.DestinationPos.Y}");

            while (!passenger.IsAtDestination())
            {
                await Tick(car, passenger);
            }

            Console.WriteLine("Destination reached!");
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static async Task Tick(Car car, Passenger passenger)
        {

            if (!car.HasPickedUpPassenger(passenger) && !CityPosition.HasCarReachedPassgenger(car, passenger))
            {
                //move towards passenger
                car.MoveTowardsPassenger(passenger);
            }
            else if (!car.HasPickedUpPassenger(passenger) && CityPosition.HasCarReachedPassgenger(car, passenger))
            {
                passenger.GetInCar(car);
            }
            else if (!CityPosition.CheckCoordinateOverlap(car.CurrentPosition, passenger.DestinationPos))
            {
                var veyoDownloadTask = DownnloadVeyoAsync();

                car.MoveTowardsPassengerDestination();

                var r = await veyoDownloadTask;
                Console.WriteLine($"Download Veyo website completed with status code: {r.StatusCode}");
            }
            else
            {
                Console.WriteLine("Destination reached!");
            }

            
        }

        public static async Task<HttpResponseMessage> DownnloadVeyoAsync()
        {
            var client = new HttpClient();
            Console.WriteLine("Downloading veyo website");
            return await client.GetAsync(@"http://www.veyo.com");
        }

    }
}
