﻿
using System;
using System.Runtime.Remoting.Messaging;

namespace TransportProj
{
    public class CityPosition
    {
        public Car Car { get; set; }
        public Passenger Passenger { get; set; }

        public CityPosition(Car c, Passenger p)
        {
            Car = c;
            Passenger = p;
        }

        public static bool HasCarReachedPassgenger(Car car, Passenger passenger)
            => CheckCoordinateOverlap(car.CurrentPosition, passenger.StartingPosition);

        public static bool CheckCoordinateOverlap(Coordinate coordinate1, Coordinate coordinate2) =>
            ((coordinate1.X == coordinate2.X) && (coordinate1.Y == coordinate2.Y));
        
    }
}
