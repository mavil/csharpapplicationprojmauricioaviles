﻿using System;

namespace TransportProj
{
    public abstract class Car
    {
        public Coordinate CurrentPosition { get; set; }

        public Passenger Passenger { get; private set; }
        public City City { get; private set; }

        internal int Speed = 1;

        public Car(int xPos, int yPos, City city, Passenger passenger)
        {
            CurrentPosition = new Coordinate() {X = xPos, Y = yPos};
            City = city;
            Passenger = passenger;
        }

        internal Car(int xPos, int yPos, City city, Passenger passenger, int speed)
        {
            CurrentPosition = new Coordinate() { X = xPos, Y = yPos };
            City = city;
            Passenger = passenger;
            Speed = speed;
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine($"Car moved to x - {CurrentPosition.X} y - {CurrentPosition.Y}");

        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
        }

        /// <summary>
        /// Not thread safe.
        /// </summary>
        public virtual void MoveUp()
        {
            if (CurrentPosition.Y < City.YMax)
            {
                //var approachSpeed = AdjustSpeedForApproach(Speed, CurrentPosition.Y, Passenger.DestinationPos.Y);
                MoveYAxisPosition(Speed);
                WritePositionToConsole();
            }
        }

        public virtual void MoveDown()
        {
            if (CurrentPosition.Y > 0)
            {
                //var approachSpeed = AdjustSpeedForApproach(Speed, CurrentPosition.Y, Passenger.DestinationPos.Y);
                MoveYAxisPosition(-Speed);
                WritePositionToConsole();
            }
        }

        public virtual void MoveRight()
        {
            if (CurrentPosition.X < City.XMax)
            {
                //var approachSpeed = AdjustSpeedForApproach(Speed, CurrentPosition.X, Passenger.DestinationPos.X);
                MoveXAxisPosition(Speed);
                WritePositionToConsole();
            }
        }

        public virtual void MoveLeft()
        {
            if (CurrentPosition.X > 0)
            {
                //var approachSpeed = AdjustSpeedForApproach(Speed, CurrentPosition.X, Passenger.DestinationPos.X);
                MoveXAxisPosition(-Speed);
                WritePositionToConsole();
            }
        }

        private int AdjustSpeedForApproach(int currentPosition, int destination)
        {
            if (currentPosition + Speed <= destination) return Speed;

            return destination - currentPosition;
        }

        private void MoveYAxisPosition(int speed)
        {
            var approachSpeed = speed;

            if (Passenger != null)
                approachSpeed = AdjustSpeedForApproach(CurrentPosition.Y, Passenger.DestinationPos.Y);

            CurrentPosition.Y += approachSpeed;
        }

        private void MoveXAxisPosition(int speed)
        {
            var approachSpeed = speed;

            if (Passenger != null)
                approachSpeed = AdjustSpeedForApproach(CurrentPosition.X, Passenger.DestinationPos.X);

            CurrentPosition.X += approachSpeed;
        }

        public bool HasPickedUpPassenger(Passenger passenger)
        {
            if (Passenger == null)
                return false;
            else
            {
                return Passenger == passenger;
            }
        }

        

        private int  distanceToPassengerOnXaxis(Coordinate c) => CurrentPosition.X - c.X;
        private int distanceToPassengerOnYaxis(Coordinate c) => CurrentPosition.Y - c.Y;


        public virtual void MoveTowardsPassenger(Passenger passenger)
        {
            WritePositionToConsole();

            MoveTowardsCoordinate(passenger.StartingPosition);

            WritePositionToConsole();
        }

        private void MoveTowardsCoordinate(Coordinate target)
        {
            //since this is a discrete operation, we move on operation at a time
            if (distanceToPassengerOnXaxis(target) != 0)
            {
                ApproachFromXaxis(target);
                return;//on operation at a time
            }

            if (distanceToPassengerOnYaxis(target) != 0)
            {
                ApproachFromYaxis(target);
            }
        }

        private void ApproachFromYaxis(Coordinate passenger)
        {
            if (distanceToPassengerOnYaxis(passenger) > 0)
            {
                MoveDown();
                return;
            }
            MoveUp();
        }

        private void ApproachFromXaxis(Coordinate passenger)
        {
            if (distanceToPassengerOnXaxis(passenger) > 0)
            {
                MoveLeft();
                return;
            }
            if (distanceToPassengerOnXaxis(passenger) < 0)
                MoveRight();
        }

        public void MoveTowardsPassengerDestination()
        {
            if(Passenger == null)
                throw new InvalidOperationException($"Car must have a passenger to invoke method: {nameof(MoveTowardsPassengerDestination)}");
            else
            {
                MoveTowardsCoordinate(Passenger.DestinationPos);
            }
        }
    }
}
