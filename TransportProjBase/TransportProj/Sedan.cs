﻿using System;

namespace TransportProj
{
    public class Sedan : Car
    {
        public Sedan(int Xpos, int Ypos, City city, Passenger passenger) : base(Xpos, Ypos, city, passenger, 1)
        {
        }

        
        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Sedan moved to x - {0} y - {1}", CurrentPosition.X, CurrentPosition.Y));
            if (Passenger != null)
                Console.WriteLine("Sedan has passenger onboard");
        }
    }
}
