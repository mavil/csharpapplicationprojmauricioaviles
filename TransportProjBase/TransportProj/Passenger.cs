﻿using System;

namespace TransportProj
{
    public class Passenger
    {
        public Coordinate StartingPosition { get; private set; }

        public Coordinate DestinationPos { get; private set; }
        public Car Car { get; set; }
        public City City { get; private set; }

        public Passenger(int startXPos, int startYPos, int destXPos, int destYPos, City city)
        {
            StartingPosition = new Coordinate() {X = startXPos, Y = startYPos};
            DestinationPos = new Coordinate() {X = destXPos, Y = destYPos};
            City = city;
        }

        public void GetInCar(Car car)
        {
            Car = car;
            car.PickupPassenger(this);
            Console.WriteLine("Passenger got in car.");
        }

        public void GetOutOfCar()
        {
            Car = null;
        }

        public int GetCurrentXPos()
        {
            if(Car == null)
            {
                return StartingPosition.X;
            }
            else
            {
                return Car.CurrentPosition.X;
            }
        }

        public int GetCurrentYPos()
        {
            if (Car == null)
            {
                return StartingPosition.Y;
            }
            else
            {
                return Car.CurrentPosition.Y;
            }
        }

        public bool IsAtDestination()
        {
            return GetCurrentXPos() == DestinationPos.X && GetCurrentYPos() == DestinationPos.Y;
        }
    }
}
