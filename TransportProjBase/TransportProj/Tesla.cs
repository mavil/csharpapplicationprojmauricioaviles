﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class Tesla : Car
    {
        public Tesla(int Xpos, int Ypos, City city, Passenger passenger) : base(Xpos, Ypos, city, passenger, 2) //This last argument sets the car speed. Teslas are fast!
        {
        }
    }
}
