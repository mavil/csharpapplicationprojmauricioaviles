﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.Dsl;
using TransportProj;

namespace TransportProjTests
{
    [TestClass]
    public class CarTests
    {
        private int irrelevantIntValue = 1;
        private Fixture fixture;
        private City City = new City(100, 100);


        [TestInitialize]
        public void Initialization()
        {
            fixture = new Fixture();
            //Needed by move related methods, initialization sets values
            City = new City(100, 100);
        }

        [TestMethod]
        public void Knows_when_there_is_NO_passenger_in_car()
        {
            var somePassenger = new Passenger(irrelevantIntValue, irrelevantIntValue, irrelevantIntValue, irrelevantIntValue, null);
            
            var SUT = new Sedan(irrelevantIntValue, irrelevantIntValue, City, null);

            Assert.IsFalse(SUT.HasPickedUpPassenger(somePassenger));
        }

        [TestMethod]
        public void Knows_when_thre_IS_passenger_in_car()
        {
            var somePassenger = new Passenger(irrelevantIntValue, irrelevantIntValue, irrelevantIntValue, irrelevantIntValue, null);
            var SUT = new Sedan(irrelevantIntValue, irrelevantIntValue, City, null);

            SUT.PickupPassenger(somePassenger);

            Assert.IsTrue(SUT.HasPickedUpPassenger(somePassenger));
        }

        [TestMethod]
        public void MoveTowardsPassenger_does_not_move_car_when_car_is_At_Passenger_Coordinates()
        {
            const int samePos = 1;
            //car and passenger are in same location, but passenger has yet to be picked up
            var car = new Sedan(samePos, samePos, null, null);
            var passengerOnFoot = new Passenger(samePos, samePos, 0, 0, null);

            car.MoveTowardsPassenger(passengerOnFoot);

            Assert.AreEqual(1, car.CurrentPosition.X);
            Assert.AreEqual(1, car.CurrentPosition.Y);
        }

        [TestMethod]
        public void MoveTowardsPassenger_moves_in_correct_X_axis_direction_towards_Passenger_Coordinates()
        {
            const int samePos = 1;

            //car and passenger are in same location, but passenger has yet to be picked up
            var car = new Sedan(samePos, samePos, City, null);
            var passengerOnFoot = new Passenger(2, samePos, 0, 0, City);

            //current implementation always moves X axis first
            car.MoveTowardsPassenger(passengerOnFoot);

            Assert.AreEqual(2, car.CurrentPosition.X);
            Assert.AreEqual(1, car.CurrentPosition.Y);
        }

        [TestMethod]
        public void MoveTowardsPassenger_moves_in_correct_Y_axis_direction_towards_Passenger_Coordinates()
        {
            const int samePos = 1;

            //car and passenger are in same location, but passenger has yet to be picked up
            var car = new Sedan(samePos, samePos, City, null);
            var passengerOnFoot = new Passenger(samePos, 2, 0, 0, City);

            //current implementation always moves X axis first
            car.MoveTowardsPassenger(passengerOnFoot);

            Assert.AreEqual(1, car.CurrentPosition.X);
            Assert.AreEqual(2, car.CurrentPosition.Y);
        }

  
    }
}
