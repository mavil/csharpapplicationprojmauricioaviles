﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransportProj;

namespace TransportProjTests
{
    //SUT -> System Under Test
    using SUT = TransportProj.CityPosition;

    [TestClass]
    public class CityPositionTests
    {
        [TestMethod]
        public void Knows_when_two_objects_are_in_same_position()
        {
            var irrelevantValue = 4;

            var city = new City(10,10);

            var passengerXPos = 5;
            var passengerYPos = 5;

            var passenger = new Passenger(passengerYPos, passengerXPos, irrelevantValue, irrelevantValue, null);

            int carxPos = 5;
            int carYPos = 5;

            var car = new Sedan(carxPos, carYPos, city, passenger);

            Assert.IsTrue(SUT.HasCarReachedPassgenger(car, passenger));
        }

        [TestMethod]
        public void Knows_when_two_objects_are_NOT_in_same_position()
        {
            var irrelevantValue = 4;

            var city = new City(10, 10);

            var passengerXPos = 4;
            var passengerYPos = 1;

            var passenger = new Passenger(passengerYPos, passengerXPos, irrelevantValue, irrelevantValue, city);
            
            //A position different than passenger
            int carxPos = 5;
            int carYPos = 5;

            var car = new Sedan(carxPos, carYPos, city, passenger);
            

            Assert.IsFalse(SUT.HasCarReachedPassgenger(car, passenger));
        }

    }
}
