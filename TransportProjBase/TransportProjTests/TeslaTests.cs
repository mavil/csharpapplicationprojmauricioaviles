﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ploeh.AutoFixture;
using TransportProj;

namespace TransportProjTests
{
    [TestClass]
    public class TeslaTests
    {
        const int initialPosition = 5;
        private City City = new City(100, 100);


        [TestMethod]
        public void MovesRight_at_correct_speed()
        {
            var SUT = new Tesla(initialPosition, initialPosition, City, null);

            SUT.MoveRight();

            //Tesla is not only electric, it is algo fast! Fast cars should move two coordinates at a time
            Assert.AreEqual(initialPosition +2, SUT.CurrentPosition.X);
        }

        [TestMethod]
        public void MovesLeft_at_correct_speed()
        {
            var SUT = new Tesla(initialPosition, initialPosition, City, null);

            SUT.MoveLeft();

            //Tesla is not only electric, it is algo fast! Fast cars should move two coordinates at a time
            Assert.AreEqual(initialPosition - 2, SUT.CurrentPosition.X);
        }

        [TestMethod]
        public void MovesUp_at_correct_speed()
        {
            var SUT = new Tesla(initialPosition, initialPosition, City, null);

            SUT.MoveUp();

            //Tesla is not only electric, it is algo fast! Fast cars should move two coordinates at a time
            Assert.AreEqual(initialPosition + 2, SUT.CurrentPosition.Y);
        }

        [TestMethod]
        public void MovesDown_at_correct_speed()
        {
            var SUT = new Tesla(initialPosition, initialPosition, City, null);

            SUT.MoveDown();

            //Tesla is not only electric, it is algo fast! Fast cars should move two coordinates at a time
            Assert.AreEqual(initialPosition - 2, SUT.CurrentPosition.Y);
        }

        [TestMethod]
        public void Can_adjust_speed_to_reach_passenger_destination()
        {
            int passengerDestination = 6;

            var SUT = new Tesla(initialPosition, initialPosition, City, null);

            var passanger = new Passenger(initialPosition, initialPosition, passengerDestination, passengerDestination, City);

            passanger.GetInCar(SUT);

            SUT.MoveTowardsPassengerDestination();
            SUT.MoveTowardsPassengerDestination();

            Assert.AreEqual(6, SUT.CurrentPosition.X);
            Assert.AreEqual(6, SUT.CurrentPosition.Y);
        }

        [TestMethod]
        public void Can_adjust_speed_to_reach_passenger_destination_in_negative_direction()
        {
            int startingPosition = 6;
            int passengerDestination = 5;

            var SUT = new Tesla(startingPosition, startingPosition, City, null);

            var passanger = new Passenger(startingPosition, startingPosition, passengerDestination, passengerDestination, City);

            passanger.GetInCar(SUT);

            SUT.MoveTowardsPassengerDestination();
            SUT.MoveTowardsPassengerDestination();

            Assert.AreEqual(5, SUT.CurrentPosition.X);
            Assert.AreEqual(5, SUT.CurrentPosition.Y);
        }

    }
}
